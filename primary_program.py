# -*- coding: utf-8 -*-
"""
Created on Thu Nov 10 10:17:21 2016

@author: franko
"""
from fipy import *
import os.path
import sys

def main():
    name = raw_input("Enter the name of '.msh' file> ")
    if os.path.isfile(name+'.msh'):
        mesh = Gmsh2D(name+'.msh')

    else:
        print "Invalide '.msh' file name, aborting program"
        return        
    
    if os.path.isfile(name+'.txt'):
        BoundaryFile = open(name+'.txt', 'r')    
    else:
        print "Invalide '.txt' file name, aborting program"
        return        
    
            
    BoundaryNames = []
    BoundaryValues = []
    for BoundaryData in BoundaryFile.readlines():
        BoundaryData = BoundaryData.split()        
        BoundaryNames.append(BoundaryData[0])
        BoundaryValues.append(float(BoundaryData[1]))

    phi = CellVariable(name = "solution variable", mesh = mesh)     
    for boundary,value in zip(BoundaryNames,BoundaryValues):
        phi.constrain(value,  where = mesh.physicalFaces[boundary])
    
    viewer = None
    viewer = Viewer(vars=phi, datamin = int(min(BoundaryValues)), datamax =int(max(BoundaryValues)))
    DiffusionTerm(var=phi).solve()
    viewer.plot()
    
    
    

if __name__ == "__main__":
    main()
