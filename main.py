# -*- coding: utf-8 -*-
"""
Main program for testing and writing examples
"""

import os
from interaction import CreateGeo

myGeo = CreateGeo()
#Cell size
print "Enter the cell size:"
size = raw_input("> ")
myGeo.set_cell_size(float(size))
#Points input
print "Enter the x,y,z cordinates, for exit enter 'q':"
points = []
while True:
    data = raw_input("> ").split()
    dot =  []  
    if data[0] == 'q':
        break
    else:
        dot.append(float(data[0]))
        dot.append(float(data[1]))
        dot.append(float(data[2]))
        dot.append(myGeo.get_cell_size())        
        points.append(dot)

myGeo.set_points(points)
myGeo.print_points()
 
#Entering lines
print "\nEnter your lines by entering start and ending point_id. For exit enter 'q'"
lines = []
while True:
    data = raw_input("> ").split()
    line = []
    if data[0] == 'q':
        break
    else:
        line.append(int(data[0]))
        line.append(int(data[1]))
        lines.append(line)

myGeo.set_lines(lines)
myGeo.print_lines()

#Enter boundaries:
boundaries = []

print "Enter boundaries for lines:\n"
for i,line in enumerate(lines):
    bound = raw_input("Line %d boundary> " %(i+1))
    boundaries.append(bound)

myGeo.crete_geo('myGeo')
myGeo.set_boundaries(boundaries)
myGeo.preview_mesh()