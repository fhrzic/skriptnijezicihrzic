# -*- coding: utf-8 -*-
"""
Created on Wed Nov  2 17:04:57 2016

@author: franko

"""
from fipy import *
from numpy import ones,vstack
from numpy.linalg import lstsq

#myMesh = Gmsh2D('myGeo.msh',communicator=serialComm)
mesh = Gmsh2D('untitled.msh')

phi = CellVariable(name = "solution variable", mesh = mesh)
left = mesh.facesLeft()
right = mesh.facesRight()
top = mesh.facesTop()
bottom = mesh.facesBottom()

print mesh.cellFaceIDs
vrijednosti = [False] * len(mesh.cellCenters[0])
vrijednosti[0] = True
vrijednosti[1] = True
#phi.constrain(50, where=mesh.physicalFaces["line1"].all())
#phi.constrain(50,  where = mesh.cellCenters[0] == mesh.cellCenters[0][2])
phi.constrain(50,  where = mesh.physicalFaces["line1"])
phi.constrain(20,  where = mesh.physicalFaces["line2"])

"""
phi.constrain(0, left)
phi.constrain(0, right)
phi.constrain(0, top)
phi.constrain(0, bottom)
"""

viewer = None
viewer = Viewer(vars=phi, datamin = 0.0, datamax =50.0)

DiffusionTerm(var=phi).solve()
viewer.plot()
"""
points = [(1,5),(3,4)]
x_coords, y_coords = zip(*points)
A = vstack([x_coords,ones(len(x_coords))]).T
m, c = lstsq(A, y_coords)[0]
print "Line Solution is y = {m}x + {c}".format(m=m,c=c)
"""