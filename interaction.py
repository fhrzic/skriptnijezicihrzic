# -*- coding: utf-8 -*-
"""
Class that accepts data and creates .geo file
"""

from numpy import ones,vstack
import numpy as np
from numpy.linalg import lstsq
from fipy import *
import math as mth

class CreateGeo:
    
    def __init__(self):
        self.points = []
        self.lines = []
    
    def print_points(self):
        print '\nYour points are:'
        print 'Point_id'+(' ')*3+'x'+(' ')*5+'y'+(' ')*5+'z'+(' ')*5+'d'
        for i,d in enumerate(self.points):
            print ' %d %12.2f %5.2f %5.2f %5.2f' % (i+1,d[0],d[1],d[2],d[3])  
               
    def print_lines(self):
        print '\nYour lines are:'
        print 'Line_id'+(' ')*5+'Start point id'+(' ')*5+'End point id'
        for i,line in enumerate(self.lines):
            print '   %d %15d %17d' %(i+1,line[0],line[1])

    def get_cell_size(self):
        return self.cell_size
        
    def set_cell_size(self, size):
        self.cell_size = size
      
    def set_points(self,points):
        self.points = points

    def set_lines(self,lines):
        self.lines = lines
    
    def genereate_line_loop(self):
        lines = self.lines
        line_loop = []
        line_loop.append(1)
        begin_line = lines[0] 
        begin_point = begin_line[1]
        start = 1
        counter = 0
        while True:
            line_number = 0
            for i in range(start,len(lines)):
                line_number = i+1              
                if begin_point in lines[i] and line_number not in line_loop and line_number*(-1) not in line_loop:
                    start = 0
                    begin_line = lines[i]
                    if begin_point != lines[i][0]:
                        line_number = line_number*(-1)
                        begin_point = begin_line[0]
                    else:
                        begin_point = begin_line[1]
                    line_loop.append(line_number)
                    break
            counter += 1
            if counter == (len(lines)-1):
                break
        return line_loop                            
                        
    def crete_geo(self, name):
        f = open(name+'.geo', 'w')

        for i,point in enumerate(self.points):
            f.write('Point(%d) = {%f, %f, %f, %f};\n' % (i+1,point[0],point[1],point[2],point[3]))
        
        for i,line in enumerate(self.lines):
            f.write('Line(%d) = {%d, %d};\n' % (i+1,line[0], line[1]))
        
        line_ids = self.genereate_line_loop()
        line_loop = 'Line Loop(%d) = {' % int(len(self.lines)+2)
                
        for ids in line_ids:
            line_loop += str(ids)+', '
            
        line_loop = line_loop[:-2]+'};\n'
        f.write(line_loop)
        
        f.write('Plane Surface(1) = {%d};\n' % (len(self.lines)+2))
        f.write('Physical Surface(1) = {1};\n')
        
        line_ids = [i+1 for i in range(len(self.lines))]
        for count_line, lid in enumerate(line_ids):
            count_line +=1
            f.write('Physical Line(%d) = {%d};\n' % (count_line, lid))

        f.close()
    
    def generate_cell_list(self,line_num):
        my_line = self.lines[line_num]
        my_point1 = self.points[my_line[0]-1]
        my_point2 = self.points[my_line[1]-1]
        points = [(my_point1[0],my_point1[1]),(my_point2[0],my_point2[1])]
        
        x_coords, y_coords = zip(*points)
        A = vstack([x_coords,ones(len(x_coords))]).T
        m, c = lstsq(A, y_coords)[0]

        if my_point1[0]>my_point2[0]:
            x2,x1 = my_point1[0],my_point2[0]
        else:
            x1,x2 = my_point1[0],my_point2[0]
        
        x_cor = np.arange(x1+self.cell_size/2,x2,self.cell_size)
        y_cor = m * x_cor + c         
        if x_cor.size == 0:
            if my_point1[1]>my_point2[1]:
                y2,y1 = my_point1[1],my_point2[1]
            else:
                y1,y2 = my_point1[1],my_point2[1]
            y_cor = np.arange(y1+self.cell_size/2,y2,self.cell_size)
            x_cor = np.ones(len(y_cor))*my_point1[0]
               
        
        cor_list = zip(x_cor,y_cor)
        self.load_mesh()
        
        list_of_cells = []        
        x_cell_general,y_cell_general = self.mesh.cellCenters()
        for x,y in zip(x_cor,y_cor):
            index = 0
            minimum_distance = 10000
            cell_index = 0
            for x_cell,y_cell in zip(x_cell_general,y_cell_general):      
                distance = mth.sqrt((x-x_cell)**2 + (y-y_cell)**2)
                if distance < minimum_distance:
                    cell_index = index
                    minimum_distance = distance
                index += 1
            list_of_cells.append(cell_index)
        
        return list_of_cells
   
    def load_mesh(self):
        self.mesh = Gmsh2D('myGeo.geo')
      
     
    def preview_mesh(self):
        phi = CellVariable(name = "solution variable", mesh = self.mesh)
        
        for boundary,faces in zip(self.boundaries,self.general_boundary_list):       
            phi.constrain(boundary,faces)
        
        viewer = None
        viewer = Viewer(vars=phi, datamin = int(min(self.boundaries)), datamax = int(max(self.boundaries)))
        DiffusionTerm(var=phi).solve()
        viewer.plot()
        
    def set_boundaries(self,boundaries):
        self.boundaries = boundaries
        self.load_mesh()
        self.general_boundary_list = []  
        exterior_list = self.mesh.exteriorFaces()
        for line_num, boundary in enumerate(self.boundaries):
            list_of_cells = []            
            list_of_cells = self.generate_cell_list(line_num)
            list_of_faces = []
            
            for cell in list_of_cells:
                list_of_faces.append(self.mesh.cellFaceIDs[0][cell])
                list_of_faces.append(self.mesh.cellFaceIDs[1][cell])
                list_of_faces.append(self.mesh.cellFaceIDs[2][cell])

            tmp_list = [False] * self.mesh.numberOfFaces
            for bound in list_of_faces:
                tmp_list[bound] = True

            for i,each in enumerate(exterior_list):
                if tmp_list[i] == True and each == False:
                    tmp_list[i] = False                    
            
            self.general_boundary_list.append(tmp_list)
            